const createInput = document.createElement('div');
createInput.textContent = 'Price'
const inputPrice = document.createElement('input')
createInput.append(inputPrice)
document.body.append(createInput)

inputPrice.addEventListener('focusin', () => {
    inputPrice.classList.add('green')

})

inputPrice.addEventListener('focusout', () => {
    inputPrice.classList.remove('green')

    if (inputPrice.value < 0){
        inputPrice.classList.add('red')
        createSpan()
    }else {
        createSpan(inputPrice.value)
        inputPrice.classList.add('green')
    }

})

const createSpan = function (value) {
    if (value){
        const invalid = document.createElement('span');
        invalid.textContent = 'Текущая цена: ' + value

        const btnX = document.createElement('button');
        btnX.textContent = 'X'
        btnX.addEventListener("click", function () {
            invalid.remove();
            inputPrice.value = ''
            inputPrice.classList.remove('green')
        } )

        invalid.append(btnX)

        document.body.before(invalid)
    }else {
        const invalid = document.createElement('span');
        invalid.textContent = 'Please enter correct price'
        document.body.after(invalid)
    }
}
